<?php

namespace Drupal\signageos\Commands;

use Drupal\digital_signage_platform\PlatformPluginManager;
use Drush\Commands\DrushCommands;

/**
 * Abstract class for drush commands.
 */
abstract class Base extends DrushCommands {

  /**
   * @var \Drupal\signageos\Plugin\DigitalSignagePlatform\SignageOs
   */
  protected $plugin;

  /**
   * AppletCommands constructor.
   *
   * @param \Drupal\digital_signage_platform\PlatformPluginManager $platform_plugin_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(PlatformPluginManager $platform_plugin_manager) {
    parent::__construct();
    $this->plugin = $platform_plugin_manager->createInstance('signageos');
  }

}
