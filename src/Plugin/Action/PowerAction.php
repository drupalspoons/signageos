<?php

namespace Drupal\signageos\Plugin\Action;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Execute power actions on devices.
 *
 * @Action(
 *   id = "signageos_power_action",
 *   label = @Translation("signageOS device Power Action"),
 *   type = "digital_signage_device"
 * )
 */
class PowerAction extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResultAllowed::allowedIf($account !== NULL && $account->hasPermission('execute signageos power action'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'action' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Power action'),
      '#default_value' => $this->configuration['action'],
      '#options' => [
        'APP_RESTART' => $this->t('Restart'),
        'SYSTEM_REBOOT' => $this->t('Reboot'),
        'APPLET_RELOAD' => $this->t('Reload'),
        'APPLET_REFRESH' => $this->t('Refresh'),
        'DISPLAY_POWER_ON' => $this->t('Display power on'),
        'DISPLAY_POWER_OFF' => $this->t('Display power off'),
        'APPLET_DISABLE' => $this->t('Applet disable'),
        'APPLET_ENABLE' => $this->t('Applet enable'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['action'] = $form_state->getValue('action');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($device = NULL) {
    if ($device->bundle() !== 'signageos') {
      return;
    }
    $device->getPlugin()->sendPowerAction($device, $this->configuration['action']);
  }

}
